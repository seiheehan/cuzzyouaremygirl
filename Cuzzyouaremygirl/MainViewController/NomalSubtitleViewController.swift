//
//  NomalSubtitleViewController.swift
//  Cuzzyouaremygirl
//
//  Created by seihee han on 16/11/2018.
//  Copyright © 2018 seihee han. All rights reserved.
//

import UIKit
import AnimatedTextInput
import GoogleMobileAds

class NomalSubtitleViewController: UIViewController, AnimatedTextInputDelegate, GADInterstitialDelegate {

    @IBOutlet weak var bodyTextField: AnimatedTextInput!
    @IBOutlet weak var fontsizeTextField: AnimatedTextInput!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var constraintWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    
    var selectedImg: UIImage = UIImage()
    var interstitial: GADInterstitial!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bodyTextField.delegate = self
        self.fontsizeTextField.delegate = self
        // Do any additional setup after loading the view.
        
        setTextInput()
        setupKeyboardDismissRecognizer()
        printFonts()
        
        self.interstitial = createAndLoadInterstitial()
    }
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
//            println("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    override func viewDidLayoutSubviews() {
        setImage(image: selectedImg)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupKeyboardDismissRecognizer(){
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    func setImage(image: UIImage) {
        previewImageView.image = resizeImageWithAspect(image: image, scaledToMaxWidth: previewImageView.bounds.width, maxHeight: previewImageView.bounds.height)
        
        let screenSize = containerView.frame.size
        
        let imageAspectRatio = image.size.width / image.size.height
        let screenAspectRatio = screenSize.width / screenSize.height
        
        if imageAspectRatio > screenAspectRatio {
            constraintWidth.constant = min(image.size.width, screenSize.width)
            constraintHeight.constant = constraintWidth.constant / imageAspectRatio
        }
        else {
            constraintHeight.constant = min(image.size.height, screenSize.height)
            constraintWidth.constant = constraintHeight.constant * imageAspectRatio
        }
        
        view.layoutIfNeeded()
    }
    
    func resizeImageWithAspect(image: UIImage,scaledToMaxWidth width:CGFloat,maxHeight height :CGFloat) -> UIImage? {
        let oldWidth = image.size.width;
        let oldHeight = image.size.height;
        
        let scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
        
        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize,false,UIScreen.main.scale);
        
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage
    }
    
    // MARK: - TextField 설정
    func setTextInput() {
        bodyTextField.type = .multiline
        fontsizeTextField.type = .numeric
        
        bodyTextField.style = CustomTextInputStyle()
        fontsizeTextField.style = CustomTextInputStyle()
        
        bodyTextField.placeHolderText = "자막을 입력 하세요";
        fontsizeTextField.placeHolderText = "폰트 사이즈를 입력 하세요";
        
        fontsizeTextField.text = "30"
    }
    
    func animatedTextInputDidChange(animatedTextInput: AnimatedTextInput) {
        // max 2line
        if animatedTextInput.tag == 0 {
            let strokeTextAttributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.strokeColor : UIColor.black,
                NSAttributedString.Key.foregroundColor : UIColor.white,
                NSAttributedString.Key.strokeWidth : -2.0,
                ]
            bodyLabel.attributedText = NSMutableAttributedString(string: bodyTextField.text!, attributes: strokeTextAttributes)
        }
        else if animatedTextInput.tag == 1 {
            if let size: Float = Float(fontsizeTextField.text!) {
                bodyLabel.font =  bodyLabel.font.withSize(CGFloat(size))
            }
        }
    }
    
    @IBAction func handlePan(recognizer:UIPanGestureRecognizer) {
        let trans = recognizer.translation(in: self.view)
        if let view = recognizer.view {
            view.center = CGPoint(x: view.center.x + trans.x,
                                  y: view.center.y + trans.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func handleRotate(recognizer:UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    @IBAction func saveImg(_ sender: Any) {
        if let saveImg: UIImage = imageWithView(view: containerView, imgView: previewImageView) {
            print("save")
            UIImageWriteToSavedPhotosAlbum(saveImg, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil);
        }
    }
    
    func imageWithView(view: UIView, imgView: UIImageView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(imgView.bounds.size, imgView.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "완료", message: "감성을 저장했습니다", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action: UIAlertAction) in
                if self.interstitial.isReady {
                    self.interstitial.present(fromRootViewController: self)
                }
            }))
            
            present(alert, animated: true)
            // Human theater
        }
    }
    
    @IBAction func changeFont(_ sender: Any) {
        
        let alert = UIAlertController(title: "폰트를 선택 하세요", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "일반", style: .default , handler:{ (UIAlertAction)in
            if let size: Float = Float(self.fontsizeTextField.text!) {
                self.bodyLabel.font = UIFont.boldSystemFont(ofSize: CGFloat(size))
            }
        }))
        
        alert.addAction(UIAlertAction(title: "궁서", style: .default , handler:{ (UIAlertAction)in
            if let size: Float = Float(self.fontsizeTextField.text!) {
                self.bodyLabel.font = UIFont(name: "BareunBatang", size: CGFloat(size))
            }
        }))
        
        alert.addAction(UIAlertAction(title: "취소", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        }
        else {
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        }
    }
    
    // MARK: - Google AdMob
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-4582716621646848/7302117245")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        self.navigationController?.popToRootViewController(animated: true)
    }
}
