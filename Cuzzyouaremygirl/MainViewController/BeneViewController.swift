//
//  BeneViewController.swift
//  Cuzzyouaremygirl
//
//  Created by seiheehan on 06/11/2018.
//  Copyright © 2018 seihee han. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BeneViewController: UIViewController, GADInterstitialDelegate {
    var selectedImg: UIImage = UIImage()
    var interstitial: GADInterstitial!
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interstitial = createAndLoadInterstitial()
    }
    
    override func viewDidLayoutSubviews() {
        setImage(image: selectedImg)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func saveImg(_ sender: Any) {
        if let saveImg: UIImage = imageWithView(view: containerView, imgView: previewImageView) {
            print("save")
            UIImageWriteToSavedPhotosAlbum(saveImg, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil);
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "완료", message: "감성을 저장했습니다", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action: UIAlertAction) in
                if self.interstitial.isReady {
                    self.interstitial.present(fromRootViewController: self)
                }
            }))
            
            present(alert, animated: true)
        }
    }
    
    func imageWithView(view: UIView, imgView: UIImageView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(imgView.bounds.size, imgView.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    // MARK: - 이미지 비율에 맞도록 UIImageView를 리사이즈
    func setImage(image: UIImage) {
        previewImageView.image = resizeImageWithAspect(image: image, scaledToMaxWidth: previewImageView.bounds.width, maxHeight: previewImageView.bounds.height)
        
        guard let sepiaImg = createSepiaImage(image: image) else { return }
        previewImageView.image = sepiaImg
        
        let screenSize = containerView.frame.size
        
        let imageAspectRatio = image.size.width / image.size.height
        let screenAspectRatio = screenSize.width / screenSize.height
        
        if imageAspectRatio > screenAspectRatio {
            constraintWidth.constant = min(image.size.width, screenSize.width)
            constraintHeight.constant = constraintWidth.constant / imageAspectRatio
        }
        else {
            constraintHeight.constant = min(image.size.height, screenSize.height)
            constraintWidth.constant = constraintHeight.constant * imageAspectRatio
        }
        
        view.layoutIfNeeded()
    }
    
    func resizeImageWithAspect(image: UIImage,scaledToMaxWidth width:CGFloat,maxHeight height :CGFloat) -> UIImage? {
        let oldWidth = image.size.width;
        let oldHeight = image.size.height;
        
        let scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
        
        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize,false,UIScreen.main.scale);
        
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage
    }
    
    // MARK: - Sepia filter
    func createSepiaImage(image: UIImage?) -> UIImage? {
        guard let previewImage: UIImage = image else { return image }
        let originalOrientation = previewImage.imageOrientation
        let scale = previewImage.scale
        
        let context = CIContext(options: nil)
        
        if let currentFilter = CIFilter(name: "CISepiaTone") {
            let beginImage = CIImage(image: previewImage)
            currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
            currentFilter.setValue(0.8, forKey: kCIInputIntensityKey)
            
            if let output = currentFilter.outputImage {
                if let cgimg = context.createCGImage(output, from: output.extent) {
                    let processedImage = UIImage(cgImage: cgimg, scale: scale, orientation: originalOrientation)
                    return processedImage
                }
            }
        }
        return nil
    }
    
    // MARK: - Google AdMob
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-4582716621646848/7302117245")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        self.navigationController?.popToRootViewController(animated: true)
    }
}
