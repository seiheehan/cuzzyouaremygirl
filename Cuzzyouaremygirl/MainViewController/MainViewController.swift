//
//  MainViewController.swift
//  Cuzzyouaremygirl
//
//  Created by seihee han on 01/11/2018.
//  Copyright © 2018 seihee han. All rights reserved.
//

import UIKit

extension UITabBarController {
    open override var childForStatusBarStyle: UIViewController? {
        return selectedViewController
    }
}

extension UINavigationController {
    open override var childForStatusBarStyle: UIViewController? {
        return visibleViewController
    }
}

class MainViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - IBAction
    @IBAction func accessCamera(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true)
        }
    }
    
    @IBAction func accessPhotoLibrary(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true)
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            showAlert(image: image)
        }
        else {
            
        }
    }
    
    func showAlert(image: UIImage) {
        let alert = UIAlertController(title: "하고싶은 일을 선택 하세요", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "꾸쥬워마이걸", style: .default , handler:{ (UIAlertAction)in
            if let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BeneViewController") as? BeneViewController {
                if let navigator = self.navigationController {
                    viewcontroller.selectedImg = image
                    navigator.pushViewController(viewcontroller, animated: true)
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "인간극장", style: .default , handler:{ (UIAlertAction)in
            if let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubtitleViewController") as? SubtitleViewController {
                if let navigator = self.navigationController {
                    viewcontroller.selectedImg = image
                    navigator.pushViewController(viewcontroller, animated: true)
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "그냥자막", style: .default , handler:{ (UIAlertAction)in
            if let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NomalSubtitleViewController") as? NomalSubtitleViewController {
                if let navigator = self.navigationController {
                    viewcontroller.selectedImg = image
                    navigator.pushViewController(viewcontroller, animated: true)
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "취소", style: .cancel, handler:{ (UIAlertAction)in
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        }
        else {
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
        }
    }
}




