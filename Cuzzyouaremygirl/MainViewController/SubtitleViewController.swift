//
//  SubtitleViewController.swift
//  Cuzzyouaremygirl
//
//  Created by seihee han on 01/11/2018.
//  Copyright © 2018 seihee han. All rights reserved.
//

import UIKit
import AnimatedTextInput
import GoogleMobileAds

class SubtitleViewController: UIViewController, AnimatedTextInputDelegate, GADInterstitialDelegate {
    var interstitial: GADInterstitial!
    
    @IBOutlet weak var nameTextField: AnimatedTextInput!
    @IBOutlet weak var ageTextField: AnimatedTextInput!
    @IBOutlet weak var jobTextField: AnimatedTextInput!
    @IBOutlet weak var bodyTextField: AnimatedTextInput!
    
    @IBOutlet weak var ageJobLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var ageJobLabel2: UILabel!
    @IBOutlet weak var nameLabel2: UILabel!
    @IBOutlet weak var subTitleLabel2: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var constraintWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!

    var selectedImg: UIImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        ageTextField.delegate = self
        jobTextField.delegate = self
        bodyTextField.delegate = self
        
        setupKeyboardDismissRecognizer()
        setTextInput()
                
        self.interstitial = createAndLoadInterstitial()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        setImage(image: selectedImg)
        setDateLabel();
    }
    
    // MARK: - Keyboard dismiss
    func setupKeyboardDismissRecognizer(){
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    func setDateLabel() {
        let date = Date()
        let calender = Calendar.current
        
        let day = calender.component(.day, from: date)
        var hour = calender.component(.hour, from: date)
        let min = calender.component(.minute, from: date)
        if hour > 13 {
            hour = hour - 12
        }
        let weekDay: String? = getDayOfWeek()
        
        let currentDate: String = String(format: "%d(%@)\n%d:%d", day,weekDay!,hour,min)
        dateLabel.text = currentDate
    }
    
    func getDayOfWeek() -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: formatter.string(from: Date())) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        var day: String = ""
        
        switch weekDay {
        case 0:
            day = "토"
        case 1:
            day = "일"
        case 2:
            day = "월"
        case 3:
            day = "화"
        case 4:
            day = "수"
        case 5:
            day = "목"
        case 6:
            day = "금"
        default:
            day = "일"
        }
        
        return day
    }
    
    func subtitleGenerator(label: UILabel){
        let strokeTextAttributes = [
            NSAttributedString.Key.strokeColor : UIColor.black,
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.strokeWidth : -0.025
            
            ] as [NSAttributedString.Key : Any]
        
        let strokeTextAttributes2 = [
            NSAttributedString.Key.strokeColor : UIColor.black,
            NSAttributedString.Key.foregroundColor : UIColor.black,
            NSAttributedString.Key.strokeWidth : -15
            
            ] as [NSAttributedString.Key : Any]
        
        if label === nameLabel{
            self.nameLabel.attributedText = NSAttributedString(string: nameTextField.text!, attributes: strokeTextAttributes)
            self.nameLabel2.attributedText = NSAttributedString(string: nameTextField.text!, attributes: strokeTextAttributes2)
            
        }
        else if label === ageJobLabel {
            let ageJob: String = "(\(ageTextField.text!)) / \(jobTextField.text!)"
            
            self.ageJobLabel.attributedText = NSAttributedString(string: ageJob, attributes: strokeTextAttributes)
            self.ageJobLabel2.attributedText = NSAttributedString(string: ageJob, attributes: strokeTextAttributes2)
        }
        else {
            let subTitleLabel: String = "\"\(bodyTextField.text!)\""
            self.subTitleLabel.attributedText = NSAttributedString(string: subTitleLabel, attributes: strokeTextAttributes)
            self.subTitleLabel2.attributedText = NSAttributedString(string: subTitleLabel, attributes: strokeTextAttributes2)
        }
    }
    
    // MARK: - AnimatedTextInputDelegate
    func animatedTextInputDidChange(animatedTextInput: AnimatedTextInput) {
        // max 2line
        if animatedTextInput.tag == 0 {
            subtitleGenerator(label: nameLabel)
        }
        else if animatedTextInput.tag < 3 {
            subtitleGenerator(label: ageJobLabel)
        }
        else {
            subtitleGenerator(label: subTitleLabel)
        }
    }
    
    // MARK: - 이미지 비율에 맞도록 UIImageView를 리사이즈
    func setImage(image: UIImage) {
        previewImageView.image = resizeImageWithAspect(image: image, scaledToMaxWidth: previewImageView.bounds.width, maxHeight: previewImageView.bounds.height)
        let screenSize = containerView.frame.size
        
        let imageAspectRatio = image.size.width / image.size.height
        let screenAspectRatio = screenSize.width / screenSize.height
        
        if imageAspectRatio > screenAspectRatio {
            constraintWidth.constant = min(image.size.width, screenSize.width)
            constraintHeight.constant = constraintWidth.constant / imageAspectRatio
        }
        else {
            constraintHeight.constant = min(image.size.height, screenSize.height)
            constraintWidth.constant = constraintHeight.constant * imageAspectRatio
        }
        view.layoutIfNeeded()
    }
    
    // MARK: - TextField 설정
    func setTextInput() {
        ageTextField.type = .numeric
        bodyTextField.type = .multiline
        
        nameTextField.style = CustomTextInputStyle()
        ageTextField.style = CustomTextInputStyle()
        jobTextField.style = CustomTextInputStyle()
        bodyTextField.style = CustomTextInputStyle()
        
        nameTextField.placeHolderText = "이름";
        ageTextField.placeHolderText = "나이";
        jobTextField.placeHolderText = "직업";
        bodyTextField.placeHolderText = "대사를 잘 써야 개꿀잼. 최대 2줄";
    }
    
    func imageWithView(view: UIView, imgView: UIImageView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(imgView.bounds.size, imgView.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    @IBAction func saveImg(_ sender: Any) {
        if let saveImg: UIImage = imageWithView(view: containerView, imgView: previewImageView) {
            print("save")
            UIImageWriteToSavedPhotosAlbum(saveImg, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil);
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "완료", message: "감성을 저장했습니다", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action: UIAlertAction) in
                if self.interstitial.isReady {
                    self.interstitial.present(fromRootViewController: self)
                }
            }))
            
            present(alert, animated: true)
        }
    }
    
    func resizeImageWithAspect(image: UIImage,scaledToMaxWidth width:CGFloat,maxHeight height :CGFloat) -> UIImage? {
        let oldWidth = image.size.width;
        let oldHeight = image.size.height;
        
        let scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
        
        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize,false,UIScreen.main.scale);
        
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage
    }
    
    // MARK: - Google AdMob
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-4582716621646848/7302117245")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        self.navigationController?.popToRootViewController(animated: true)
    }
}

struct CustomTextInputStyle: AnimatedTextInputStyle {
    let placeholderInactiveColor = UIColor.gray
    let activeColor = UIColor.lightGray
    let inactiveColor = UIColor.gray.withAlphaComponent(0.3)
    let lineInactiveColor = UIColor.gray.withAlphaComponent(0.3)
    let lineActiveColor = UIColor.white
    let lineHeight: CGFloat = 1
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 14)
    let textInputFontColor = UIColor.white
    let placeholderMinFontSize: CGFloat = 9
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 25
    let topMargin: CGFloat = 20
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 10
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}
