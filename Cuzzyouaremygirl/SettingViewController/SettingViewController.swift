//
//  SettingViewController.swift
//  Cuzzyouaremygirl
//
//  Created by seihee han on 08/11/2018.
//  Copyright © 2018 seihee han. All rights reserved.
//

import UIKit
import StoreKit
import MessageUI

class SettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingTableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "SettingCell",
            for: indexPath) as! SettingTableViewCell
        
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "개발자에게 메일 보내기"
        case 1:
            cell.titleLabel.text = "고마운분"
        case 2:
            cell.titleLabel.text = "오픈소스 라이선스"
        default:
            cell.titleLabel.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        switch indexPath.row {
        case 0:
            if !MFMailComposeViewController.canSendMail() {
                print("Mail services are not available")
                return
            }
            
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients(["realmasse@gmail.com"])
            composeVC.setSubject("[꾸쥬워마이걸] 개발자에게")
            composeVC.setMessageBody("Message content.", isHTML: false)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
            
        case 1:
            if let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OtherViewController") as? OtherViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(viewcontroller, animated: true)
                    viewcontroller.isThank = true
                }
            }
        case 2:
            if let viewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OtherViewController") as? OtherViewController {
                if let navigator = self.navigationController {
                    navigator.pushViewController(viewcontroller, animated: true)
                }
            }
        default:
            break
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
}
