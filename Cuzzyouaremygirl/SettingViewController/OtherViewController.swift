//
//  OtherViewController.swift
//  Cuzzyouaremygirl
//
//  Created by seihee han on 08/11/2018.
//  Copyright © 2018 seihee han. All rights reserved.
//

import UIKit

class OtherViewController: UIViewController {
    @IBOutlet weak var textView: UITextView!
    var isThank: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if isThank {
            textView.text = "아직도 앱을 사용해주시는 유저분들에게 먼저 감사하다는 말씀을 전합니다. 소스코드를 잃어버려서 처음부터 다시 만들었어요...ㅠㅠ\n이전앱과 같은 느낌을 내기위해 최선을 다했는데 만족하실지 모르겠네요.\n물론 계속 업데이트 할 예정입니다. \n이앱을 만들기 까지 도와준 저의 와이프 선미 그리고 같은회사의 까북에게 감사의 말씀을 전합니다."
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
